# Terraria Server Scripts for Ubuntu
Scripts that I use to automatically prepare my Ubuntu cloud image to host a Terraria server
# How to do it
`cd /`

Download the file terraria_server_ubuntu_bootstrapper.sh

`sudo chmod +x terraria_server_ubuntu_bootstrapper.sh`

`sudo ./terraria_server_ubuntu_bootstrapper.sh`

After the first script is finished and you're logged into the user terraria do the following:

Download the terraria_server_ubuntu_install_important_files.sh

`chmod +x terraria_server_ubuntu_install_important_files.sh`

`./terraria_server_ubuntu_install_important_files`

You can then use the other scripts to manage your server, they should be pretty self-explanatory. Do notice that terraria-server-management.sh should not be used by you as it's used by the other scripts for automation.