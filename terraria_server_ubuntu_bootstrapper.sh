#!/bin/bash

if id "terraria" &>/dev/null; then
    printf "User terraria already exists, exitting\n"
    exit 1
else
    printf "Installing the tools necessary to manage a Terraria server on Ubuntu\n"
fi

#Installing stuff
apt update -qq > /dev/null
apt upgrade -qq > /dev/null
apt install unzip zip screen nano wget -qq > /dev/null

#Setting up users and other stuff
adduser terraria
usermod -aG sudo terraria

fallocate -l 2G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile

cp /etc/fstab /etc/fstab.backup
echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab

sysctl vm.swappiness=10
cp /etc/sysctl.conf /etc/sysctl.conf.backup
echo 'vm.swappiness = 10' | tee -a /etc/sysctl.conf

# Writing important files for the Terraria server instalation

printf "Now you will have to log in to the newly created user\n"
su - terraria