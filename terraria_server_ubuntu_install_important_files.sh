#!/bin/bash

ln -s /home/terraria/.local/share/Terraria/Worlds/ Worlds

if [ -f "install-terraria-server.sh" ]; then
	rm "install-terraria-server.sh"
fi

cat << 'EOF' > install-terraria-server.sh
#!/bin/bash

function download_zip_file () {
	if wget "$1" -q -O "terraria-server.zip"; then
		printf "Downloaded file successfully\n"
	else
		printf "Failed to download file\n"
		exit 1
	fi
}

function unzip_zip_file () {
	if unzip -qq terraria-server.zip; then
		printf "Unzipped files successfully\n"
	else
		printf "Failed to unzip the files\n"
		exit 1
	fi
	rm terraria-server.zip
	mv */Linux/* ..
	cd ..
	rm -rf tmpdownload/
	chmod +x TerrariaServer
	chmod +x TerrariaServer.bin*
}

if [ -z "$1" ]; then
	printf "Run this command with the download link to the Terraria server\n"
	exit 1
fi

if [ -d TerrariaServer ]; then
	rm -rf TerrariaServer/
fi

mkdir TerrariaServer
cd TerrariaServer

mkdir tmpdownload
cd tmpdownload

download_zip_file "$1"
unzip_zip_file

ln -s ../serverconfig.txt .
EOF

if [ -f "terraria-server-start.sh" ]; then
	rm "terraria-server-start.sh"
fi

cat << 'EOF' > terraria-server-start.sh
#!/bin/bash

screen -S terraria -dm /home/terraria/TerrariaServer/TerrariaServer -config serverconfig.txt
screen -S tm -dm /bin/bash -x /home/terraria/terraria-server-management.sh
EOF

if [ -f "terraria-server-stop.sh" ]; then
	rm "terraria-server-stop.sh"
fi

cat << 'EOF' > terraria-server-stop.sh
#!/bin/bash

screen -X -S tm quit
sleep 15

screen -S terraria -X stuff "save^M"
sleep 15
screen -S terraria -X stuff "exit^M"
sleep 15

rm -rf /home/terraria/WorldsBackup4
if [ -d "/home/terraria/WorldsBackup3" ]; then mv /home/terraria/WorldsBackup3 /home/terraria/WorldsBackup4; fi
if [ -d "/home/terraria/WorldsBackup2" ]; then mv /home/terraria/WorldsBackup2 /home/terraria/WorldsBackup3; fi
if [ -d "/home/terraria/WorldsBackup1" ]; then mv /home/terraria/WorldsBackup1 /home/terraria/WorldsBackup2; fi
cp -r /home/terraria/.local/share/Terraria/Worlds/ /home/terraria/WorldsBackup1
EOF

if [ -f "terraria-server-management.sh" ]; then
	rm "terraria-server-management.sh"
fi

cat << 'EOF' > terraria-server-management.sh
#!/bin/bash

while true; do
	sleep 12600 #3 hours and 30 minutes
	screen -S terraria -X stuff "say Server restarting in 30 minutes^M"
	sleep 1200
	screen -S terraria -X stuff "say Server restarting in 10 minutes^M"
	sleep 300
	screen -S terraria -X stuff "say Server restarting in 5 minutes^M"
	sleep 240
	screen -S terraria -X stuff "say Server restarting in 1 minute^M"
	sleep 30
	screen -S terraria -X stuff "say Server restarting in 30 seconds^M"
	sleep 20
	screen -S terraria -X stuff "say Server restarting in 10 seconds^M"
	sleep 10
	screen -S terraria -X stuff "save^M"
	sleep 15
	
	rm -rf /home/terraria/WorldsBackup4
	if [ -d "/home/terraria/WorldsBackup3" ]; then mv /home/terraria/WorldsBackup3 /home/terraria/WorldsBackup4; fi
	if [ -d "/home/terraria/WorldsBackup2" ]; then mv /home/terraria/WorldsBackup2 /home/terraria/WorldsBackup3; fi		
	if [ -d "/home/terraria/WorldsBackup1" ]; then mv /home/terraria/WorldsBackup1 /home/terraria/WorldsBackup2; fi
	cp -r /home/terraria/.local/share/Terraria/Worlds/ /home/terraria/WorldsBackup1
	
	screen -S terraria  -X stuff "exit^M"
	sleep 30
	screen -X -S terraria quit #In case the game doesn't end successfully, close the session forcibly
	screen -S terraria -dm /home/terraria/TerrariaServer/TerrariaServer -config serverconfig.txt
done
EOF

if [ -f "serverconfig.txt" ]; then
	rm "serverconfig.txt"
fi

cat << 'EOF' > serverconfig.txt
#this is an example config file for TerrariaServer.exe
#use the command 'TerrariaServer.exe -config serverconfig.txt' to use this configuration or run start-server.bat
#please report crashes by emailing crashlog.txt to support@terraria.org

#the following is a list of available command line parameters:

#-config <config file>				            Specifies the configuration file to use.
#-port <port number>				              Specifies the port to listen on.
#-players <number> / -maxplayers <number>	Sets the max number of players
#-pass <password> / -password <password>		Sets the server password
#-world <world file>					Load a world and automatically start the server.
#-autocreate <#>					Creates a world if none is found in the path specified by -world. World size is specified by: 1(small), 2(medium), and 3(large).
#-banlist <path>					Specifies the location of the banlist. Defaults to "banlist.txt" in the working directory.
#-worldname <world name>             			Sets the name of the world when using -autocreate.
#-secure						Adds addition cheat protection to the server.
#-noupnp						Disables automatic port forwarding
#-steam							Enables Steam Support
#-lobby <friends> or <private>				Allows friends to join the server or sets it to private if Steam is enabled
#-ip <ip address>					Sets the IP address for the server to listen on
#-forcepriority <priority>				Sets the process priority for this task. If this is used the "priority" setting below will be ignored.
#-disableannouncementbox				Disables the text announcements Announcement Box makes when pulsed from wire.
#-announcementboxrange <number>				Sets the announcement box text messaging range in pixels, -1 for serverwide announcements.
#-seed <seed>						Specifies the world seed when using -autocreate

#remove the # in front of commands to enable them.

#Load a world and automatically start the server.
world=/home/terraria/.local/share/Terraria/Worlds/

#Creates a new world if none is found. World size is specified by: 1(small), 2(medium), and 3(large).
#autocreate=1

#Sets the world seed when using autocreate
#seed=AwesomeSeed

#Sets the name of the world when using autocreate
#worldname=Terraria

#Sets the difficulty of the world when using autocreate 0(classic), 1(expert), 2(master), 3(journey)
#difficulty=0

#Sets the max number of players allowed on a server.  Value must be between 1 and 255
maxplayers=12

#Set the port number
port=7777

#Set the server password
password=laranjadoceu

#Set the message of the day
motd=Patrocinado por sua quenga favorita

#Sets the folder where world files will be stored
#worldpath=C:\Users\Defaults\My Documents\My Games\Terraria\Worlds\

#Sets the number of rolling world backups to keep
#worldrollbackstokeep=2

#The location of the banlist. Defaults to "banlist.txt" in the working directory.
#banlist=banlist.txt

#Adds addition cheat protection.
#secure=1

#Sets the server language from its language code. 
#English = en-US, German = de-DE, Italian = it-IT, French = fr-FR, Spanish = es-ES, Russian = ru-RU, Chinese = zh-Hans, Portuguese = pt-BR, Polish = pl-PL,
#language=en-US

#Automatically forward ports with uPNP
upnp=1

#Reduces enemy skipping but increases bandwidth usage. The lower the number the less skipping will happen, but more data is sent. 0 is off.
#npcstream=60

#Default system priority 0:Realtime, 1:High, 2:AboveNormal, 3:Normal, 4:BelowNormal, 5:Idle
priority=1

#Reduces maximum liquids moving at the same time. If enabled may reduce lags but liquids may take longer to settle.
#slowliquids=1

#Journey mode power permissions for every individual power. 0: Locked for everyone, 1: Can only be changed by host, 2: Can be changed by everyone
#journeypermission_time_setfrozen=2
#journeypermission_time_setdawn=2
#journeypermission_time_setnoon=2
#journeypermission_time_setdusk=2
#journeypermission_time_setmidnight=2
#journeypermission_godmode=2
#journeypermission_wind_setstrength=2
#journeypermission_rain_setstrength=2
#journeypermission_time_setspeed=2
#journeypermission_rain_setfrozen=2
#journeypermission_wind_setfrozen=2
#journeypermission_increaseplacementrange=2
#journeypermission_setdifficulty=2
#journeypermission_biomespread_setfrozen=2
#journeypermission_setspawnrate=2
EOF

chmod +x install-terraria-server.sh
chmod +x terraria-server-*.sh